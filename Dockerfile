#Node package is required
FROM node:16

#Create directory to app
WORKDIR /app

#Copy packages of the dependencies into directory
COPY package*.json ./

#Installing dependencies
RUN npm install

#Copy application in the same directory
COPY . .

#For running application
CMD ["npm", "start"]