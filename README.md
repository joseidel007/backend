# GBH - Demo API
- This RestApi contains a Dockerfile that allows you to run the api.
- Execute the following command with docker:

- sudo docker build -t gbh-restapi .  (This comman is for create docker image)
- sudo docker run gbh-restapi -p 3002:3002 (This comman is for run the api in the port 3002)
